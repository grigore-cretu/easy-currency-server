# Easy Currency Server (ECS)

Java/Spring Boot server component responsible for currency conversion opertaions. 

##Requirements

- [JDK 11 ](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

### Getting set up ###

Clone the project to your local repo and run:

    ./gradlew clean 
   
### Building ###

    ./gradlew build
    
### Assembling ###
     
To assemble the build artifacts, run:
       
    ./gradlew clean assemble

### Testing ###

In the root directory of the project, run:

    ./gradlew test
   
### Running ###

To run a local version of the assembled project, use the following commands:

##### Using the assembled jar file: #####

    java -jar build/libs/server-0.0.1-SNAPSHOT.jar
 
##### Using the grade task: #####
     ./gradlew bootRun
  
Otherwise, run the application from within IntelliJ IDEA using the ServerApplication.java class as the main class.


##Configuration

The server is configured via yaml configuration files in the resources directory.

**Context path and port configuration**:

Ex:
```yaml

server:
  port: 8080
  servlet:
    context-path: /ecs

```

**Third party currency rate providers configuration**:
```yaml

api:
  exchange-rate:
    rooikat-holdings:
      base-url: https://api.exchangerate-api.com
      rate-path: /v4/latest/{currencyCode}
    madis-vain:
      base-url: https://api.exchangeratesapi.io
      rate-path: /laktest?base={currencyCode}

```

**Network communcitation timeouts used to build the WebClient**:
```yaml

api:
  exchange-rate:
    rooikat-holdings:
      base-url: https://api.exchangerate-api.com
      rate-path: /v4/latest/{currencyCode}
    madis-vain:
      base-url: https://api.exchangeratesapi.io
      rate-path: /laktest?base={currencyCode}

```