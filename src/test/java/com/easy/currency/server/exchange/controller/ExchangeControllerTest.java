package com.easy.currency.server.exchange.controller;


import com.easy.currency.server.ApplicationConfig;
import com.easy.currency.server.exchange.controller.dto.ConversionRequest;
import com.easy.currency.server.exchange.controller.dto.ConversionResponse;
import com.easy.currency.server.exchange.service.CurrencyExchangeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = ExchangeController.class)
@Import(ApplicationConfig.class)
public class ExchangeControllerTest
{
    @MockBean
    private CurrencyExchangeService exchangeService;

    @Autowired
    private WebTestClient webClient;


    @Test
    void thatCurrencyExchangeEndpointSuccessfullyConvertsCurrency()
    {
        //given:
        String fromCurrrency = "EUR";
        String toCurrency = "USD";
        BigDecimal amount = BigDecimal.valueOf(11.22);
        ConversionRequest request = new ConversionRequest();
        request.setFrom(fromCurrrency);
        request.setTo(toCurrency);
        request.setAmount(amount);
        ConversionResponse expectedResponse = ConversionResponse.builder().build();

        Mockito.when(exchangeService.convertCurrency(fromCurrrency, toCurrency, amount)).thenReturn(Mono.just(expectedResponse));

        //when:
        webClient.post()
                .uri("/currency/convert")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromPublisher(Mono.just(request), ConversionRequest.class))
                .exchange()
                .expectStatus()
                .isOk();

        //then:
        Mockito.verify(exchangeService, times(1)).convertCurrency(fromCurrrency, toCurrency, amount);
    }
}
