package com.easy.currency.server.exchange.service;


import com.easy.currency.server.exchange.controller.dto.ConversionResponse;
import com.easy.currency.server.exchange.exceptions.ExchangeRateProviderException;
import com.easy.currency.server.exchange.provider.CurrencyRateProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyExchangeServiceTest
{
    private static final String USD = "USD";
    private static final String EUR = "EUR";

    @Mock
    private CurrencyRateProvider currencyRateProvider;

    @InjectMocks
    private CurrencyExchangeService exchangeService;

    @Test
    public void thatExchangeServiceSuccessfullyResolveTheCurrencyRate()
    {
        //given:
        BigDecimal amount = BigDecimal.valueOf(2.0);
        BigDecimal convertedAmount = BigDecimal.valueOf(4.0);
        BigDecimal rate = BigDecimal.valueOf(2.0);
        Mono<BigDecimal> expectedRate = Mono.just(rate);
        Mockito.when(currencyRateProvider.getExchangeRate(USD, EUR)).thenReturn(expectedRate);

        //when:
        Mono<ConversionResponse> response = exchangeService.convertCurrency(USD, EUR, amount);

        //then:
        StepVerifier.create(response)
                .consumeNextWith(conversionResponse -> {
                    assertThat(conversionResponse.getFrom()).isEqualTo(USD);
                    assertThat(conversionResponse.getTo()).isEqualTo(EUR);
                    assertThat(conversionResponse.getAmount()).isEqualTo(amount);
                    assertThat(conversionResponse.getConverted()).isEqualByComparingTo(convertedAmount);
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void thatExchangeServiceFailsToResolveTheCurrencyRate()
    {
        //given:
        BigDecimal amount = BigDecimal.valueOf(2.0);
        String errorMessage = "Currency is not supported";
        Mono<BigDecimal> error = Mono.error(new ExchangeRateProviderException(errorMessage));
        Mockito.when(currencyRateProvider.getExchangeRate(USD, EUR)).thenReturn(error);

        //when:
        Mono<ConversionResponse> response = exchangeService.convertCurrency(USD, EUR, amount);

        //then:
        StepVerifier.create(response)
                .expectErrorSatisfies(err -> {
                    assertThat(err).isInstanceOf(ExchangeRateProviderException.class);
                    assertThat(err.getMessage()).isEqualTo(errorMessage);
                })
                .verify();
    }
}
