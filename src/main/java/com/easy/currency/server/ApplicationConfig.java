package com.easy.currency.server;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Configuration
public class ApplicationConfig
{
    @Value("${api.exchange-rate.rooikat-holdings.base-url}")
    private String rookatHoldingsApi;

    @Value("${api.exchange-rate.madis-vain.base-url}")
    private String madisVainApi;

    @Value("${tcp.client.timeout.read}")
    private int tcpClientReadTimeout;

    @Value("${tcp.client.timeout.write}")
    private int tcpClientWriteTimeout;

    @Value("${tcp.client.timeout.connection}")
    private int tcpClientConnectionTimeout;


    @Bean
    public WebClient.Builder rookatHoldingsApiWebClient(TcpClient tcpClient)
    {
        return WebClient.builder()
                .baseUrl(rookatHoldingsApi)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)));
    }

    @Bean
    public WebClient.Builder madisVainApiWebClient(TcpClient tcpClient)
    {
        return WebClient.builder()
                .baseUrl(madisVainApi)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)));
    }

    @Bean
    public TcpClient tcpClient()
    {
        return TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, tcpClientConnectionTimeout * 1000)
                .doOnConnected(connection ->
                        connection.addHandlerLast(new ReadTimeoutHandler(tcpClientReadTimeout))
                                .addHandlerLast(new WriteTimeoutHandler(tcpClientWriteTimeout)));
    }
}
