package com.easy.currency.server.exchange.client.madis.vain.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class MadisVainExchangeRateApiResponse
{
    private Map<String, BigDecimal> rates;
}
