package com.easy.currency.server.exchange.client;

import com.easy.currency.server.exchange.exceptions.ExchangeRateProviderException;
import com.easy.currency.server.exchange.exceptions.NoOnlineExchangeRateProviderException;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Map;

public abstract class AbstractExchangeRateClient
{
    public abstract Mono<Map<String, BigDecimal>> getExchangeRates(final String fromCurrency);

    protected abstract WebClient.Builder getWebClientBuilder();

    protected WebClient.ResponseSpec getExchangeRateRawResponse(final String fetchRatePath, final String currencyCode)
    {
        return getWebClientBuilder().build()
            .get()
            .uri(fetchRatePath, currencyCode.toUpperCase())
            .retrieve()
            .onStatus(HttpStatus::is5xxServerError, clientResponse -> Mono.error(new NoOnlineExchangeRateProviderException("Third party currency rate proviiders are unreachable")))
            .onStatus(HttpStatus::is4xxClientError, clientResponse -> Mono.error(new ExchangeRateProviderException("The provided curerncy to convert code is not yet supported")));
    }
}
