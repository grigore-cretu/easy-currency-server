package com.easy.currency.server.exchange.service;

import com.easy.currency.server.exchange.controller.dto.ConversionResponse;
import com.easy.currency.server.exchange.provider.CurrencyRateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Service
public class CurrencyExchangeService
{
    @Autowired
    private CurrencyRateProvider exchangeRateProvider;

    public Mono<ConversionResponse> convertCurrency(final String fromCurrency, final String toCurrency, final BigDecimal amount)
    {
        final Mono<BigDecimal> exchangeRate = exchangeRateProvider.getExchangeRate(fromCurrency, toCurrency);

        return exchangeRate
                .map(rate -> rate.multiply(amount))
                .map(converted -> buildCurrencyConversionResponse(fromCurrency, toCurrency, amount, converted));
    }

    private ConversionResponse buildCurrencyConversionResponse(final String fromCurrency, final String toCurrency, final BigDecimal amount, final BigDecimal converted)
    {
        return ConversionResponse.builder().
                from(fromCurrency)
                .to(toCurrency)
                .amount(amount)
                .converted(converted)
                .build();
    }
}
