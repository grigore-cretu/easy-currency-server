package com.easy.currency.server.exchange.exceptions;

import com.easy.currency.server.exchange.exceptions.model.Error;
import com.easy.currency.server.exchange.exceptions.model.ErrorResponse;
import lombok.extern.java.Log;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ControllerAdvice
@Log
class ExchangeRateServerExceptionHandler
{

    static final String EXCHANGE_RATE_SERVICE_ERROR_CODE = "exchange.rate.server.error";
    static final String REQUEST_VALIDATION_ERROR = "request.validation.error";
    static final String NO_ONLINE_RATE_PROVIDERS_FOUND = "no.online.providers";
    static final String CURRENCY_CODE_NOT_SUPPORTED = "currency.code.not.supported";

    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    ErrorResponse handleNoOnlineExchangeRateProviderException(NoOnlineExchangeRateProviderException exception)
    {
        log.info(exception.getMessage());
        return new ErrorResponse(new Error(NO_ONLINE_RATE_PROVIDERS_FOUND, exception.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    ErrorResponse handleNoOnlineExchangeRateProviderException(ExchangeRateProviderException exception)
    {
        log.info(exception.getMessage());
        return new ErrorResponse(new Error(CURRENCY_CODE_NOT_SUPPORTED, exception.getMessage()));
    }


    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    ErrorResponse handleHttpServerErrorException(Exception exception)
    {
        log.info(exception.getMessage());
        return new ErrorResponse(new Error(EXCHANGE_RATE_SERVICE_ERROR_CODE, exception.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    ErrorResponse handleExchangeRateServerException(MethodArgumentNotValidException exception)
    {
        log.info(exception.getMessage());
        Map errorResponseProperties = exception.getBindingResult().getFieldErrors().stream().collect(
                Collectors.toMap(error -> error.getField(), error -> error.getDefaultMessage()));

        return new ErrorResponse(
                new Error(REQUEST_VALIDATION_ERROR, "The request contains invalid fields"),
                errorResponseProperties
        );

    }
}
