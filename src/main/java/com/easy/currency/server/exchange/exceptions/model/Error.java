package com.easy.currency.server.exchange.exceptions.model;

import lombok.Data;

@Data
public class Error {
  private String code;
  private String message;

  public Error(final String code)
  {
    this.code = code;
  }

  public Error(final String code, final String message)
  {
    this.code = code;
    this.message = message;
  }
}
