package com.easy.currency.server.exchange.client.rooikat.holdings;

import com.easy.currency.server.exchange.client.AbstractExchangeRateClient;
import com.easy.currency.server.exchange.client.rooikat.holdings.model.RookatHoldingsExchangeRateApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Map;

@Component
public class RooikatHoldingsExchangeRateClient extends AbstractExchangeRateClient
{
    @Value("${api.exchange-rate.rooikat-holdings.rate-path}")
    private String fetchRatePath;

    @Autowired
    private WebClient.Builder rookatHoldingsApiWebClient;

    @Override
    public Mono<Map<String, BigDecimal>> getExchangeRates(final String currencyCode)
    {
        return getExchangeRateRawResponse(fetchRatePath, currencyCode)
                .bodyToMono(RookatHoldingsExchangeRateApiResponse.class)
                .map(this::extractRatesFromApiResponse);
    }

    @Override
    protected WebClient.Builder getWebClientBuilder()
    {
        return rookatHoldingsApiWebClient;
    }


    private Map<String, BigDecimal> extractRatesFromApiResponse(final RookatHoldingsExchangeRateApiResponse response)
    {
        return response.getRates();
    }
}
