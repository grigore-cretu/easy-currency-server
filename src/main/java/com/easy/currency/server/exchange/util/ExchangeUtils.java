package com.easy.currency.server.exchange.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExchangeUtils
{
    public static List shuffleList(final List listToShuffle)
    {
        List shuffledList = new ArrayList(listToShuffle);
        Collections.shuffle(shuffledList);
        return shuffledList;
    }
}
