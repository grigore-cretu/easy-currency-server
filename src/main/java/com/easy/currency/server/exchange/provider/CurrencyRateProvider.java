package com.easy.currency.server.exchange.provider;

import com.easy.currency.server.exchange.client.AbstractExchangeRateClient;
import com.easy.currency.server.exchange.exceptions.ExchangeRateProviderException;
import com.easy.currency.server.exchange.util.ExchangeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class CurrencyRateProvider
{
    @Autowired
    private List<AbstractExchangeRateClient> abstractExchangeRateClients;

    public Mono<BigDecimal> getExchangeRate(final String fromCurrency, final String toCurrency)
    {
        var clients = ExchangeUtils.shuffleList(abstractExchangeRateClients);
        var exchangeRates = getExchangeRates(clients.iterator(), fromCurrency);
        var conversionRate = resolveConversionRate(exchangeRates, toCurrency);

        return conversionRate;
    }

    private Mono<BigDecimal> resolveConversionRate(final Mono<Map<String, BigDecimal>> exchangeRates, final String currencyCode)
    {
        return exchangeRates.map(rates -> rates.get(currencyCode))
                .switchIfEmpty(Mono.error(new ExchangeRateProviderException("Trying to convert to a not yet support currency")));
    }

    private Mono<Map<String, BigDecimal>> getExchangeRates(Iterator<AbstractExchangeRateClient> clientIterator, final String fromCurrency)
    {
        if (clientIterator.hasNext())
        {
            return clientIterator.next()
                    .getExchangeRates(fromCurrency)
                    .onErrorResume(error ->
                            getExchangeRates(clientIterator, fromCurrency).switchIfEmpty(Mono.error(error))
                    );
        }

        return Mono.empty();
    }
}
