package com.easy.currency.server.exchange.exceptions;

public class ExchangeRateProviderException extends RuntimeException
{
    public ExchangeRateProviderException(String msg)
    {
        super(msg);
    }
}