package com.easy.currency.server.exchange.client.rooikat.holdings.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class RookatHoldingsExchangeRateApiResponse
{
    private String base;
    private String date;
    private String time_last_updated;
    private Map<String, BigDecimal> rates;
}
