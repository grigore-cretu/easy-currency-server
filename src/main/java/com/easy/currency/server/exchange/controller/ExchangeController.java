package com.easy.currency.server.exchange.controller;

import com.easy.currency.server.exchange.controller.dto.ConversionRequest;
import com.easy.currency.server.exchange.controller.dto.ConversionResponse;
import com.easy.currency.server.exchange.service.CurrencyExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/currency")
public class ExchangeController
{
    @Autowired
    private CurrencyExchangeService exchangeService;

    @PostMapping(value = "/convert")
    @ResponseBody
    Mono<ConversionResponse> convertCurrency(@Valid @RequestBody ConversionRequest conversionRequest)
    {
        return exchangeService.convertCurrency(
                conversionRequest.getFrom(),
                conversionRequest.getTo(),
                conversionRequest.getAmount()
        );
    }
}