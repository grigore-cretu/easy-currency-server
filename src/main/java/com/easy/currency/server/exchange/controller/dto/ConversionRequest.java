package com.easy.currency.server.exchange.controller.dto;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
public class ConversionRequest
{
    @Pattern(regexp = "^[a-zA-Z]{3}$", message = "Currrency to be converted is not present or invalid")
    private String from;
    @Pattern(regexp = "^[a-zA-Z]{3}$", message = "Currrency to convert to is not present or invalid")
    private String to;
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 8, fraction = 8)
    private BigDecimal amount;
}
