package com.easy.currency.server.exchange.client.madis.vain;

import com.easy.currency.server.exchange.client.AbstractExchangeRateClient;
import com.easy.currency.server.exchange.client.madis.vain.model.MadisVainExchangeRateApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Map;

@Component
public class MadisVainExchangeRatesClientImpl extends AbstractExchangeRateClient
{

    @Value("${api.exchange-rate.madis-vain.rate-path")
    private String fetchRatePath;

    @Autowired
    private WebClient.Builder madisVainApiWebClient;

    @Override
    public Mono<Map<String, BigDecimal>> getExchangeRates(final String currencyCode)
    {
        return getExchangeRateRawResponse(fetchRatePath, currencyCode)
                .bodyToMono(MadisVainExchangeRateApiResponse.class)
                .map(this::extractRatesFromApiResponse);
    }

    @Override
    protected WebClient.Builder getWebClientBuilder()
    {
        return madisVainApiWebClient;
    }

    private Map<String, BigDecimal> extractRatesFromApiResponse(final MadisVainExchangeRateApiResponse response)
    {
        return response.getRates();
    }
}
