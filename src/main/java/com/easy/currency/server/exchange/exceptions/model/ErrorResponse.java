package com.easy.currency.server.exchange.exceptions.model;

import lombok.Data;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Data
public class ErrorResponse
{
    private final Error error;
    private final Long timestamp = Calendar.getInstance().getTimeInMillis();
    private final Map<String, String> properties = new HashMap<>();

    public ErrorResponse(final Error error)
    {
        this.error = error;
    }


    public ErrorResponse(final Error error, final Map<String, String> properties)
    {
        this.error = error;
        this.properties.putAll(properties);
    }

}

