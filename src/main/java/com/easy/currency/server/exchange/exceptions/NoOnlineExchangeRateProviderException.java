package com.easy.currency.server.exchange.exceptions;

public class NoOnlineExchangeRateProviderException extends RuntimeException
{
    public NoOnlineExchangeRateProviderException(String msg)
    {
        super(msg);
    }
}