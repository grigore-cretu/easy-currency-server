package com.easy.currency.server.exchange.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ConversionResponse
{
    private String from;
    private String to;
    private BigDecimal amount;
    private BigDecimal converted;
}
